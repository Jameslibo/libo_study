package com.tests;

import java.util.concurrent.TimeUnit;

import org.redisson.Redisson;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.config.Config;

import com.google.common.base.Stopwatch;


//@SpringBootConfiguration()
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes=RedissonApplication.class)
public class Apptests 
{

    
    
   public String lockKey = "testRedisson";//分布式锁的key
    
  //  @Autowired
    Redisson redisson;
    
    public Apptests() {
    	Config config = new Config();
    	config.useSingleServer().setAddress("redis://212.129.147.39:6379").setPassword("anchnet");
		this.redisson=(Redisson)Redisson.create(config);
	}


	//@org.junit.Test
    public void Distributed() throws Exception{
    	
    	test2();
    	//执行的业务代码
    	for(int i=0; i < 2; i++){
    		Stopwatch tem=Stopwatch.createStarted();
    		RLock lock = redisson.getLock(lockKey);
    		System.out.println(lock.getName());
    		System.out.println();
    		
    		
    		lock.lock(60, TimeUnit.SECONDS); //设置60秒自动释放锁  （默认是30秒自动过期）
    		int stock = Integer.parseInt(redisson.getAtomicLong("aaa").toString());
    		if(stock > 0){
    			//StringRedisTemplate.opsForValue().set("aaa",(stock-1)+"");
    			redisson.getAtomicLong("aaa").getAndAdd(-1);
    			
    			System.out.println("test2_:lockkey:"+lockKey+",stock:"+redisson.getAtomicLong("aaa").get()+"");
    		}
    		lock.unlock(); //释放锁
    		
    	}
    }
    

    public void test2(){
    	//设置一个key，aaa商品的库存数量为100
    	RAtomicLong rlong =redisson.getAtomicLong("aaa");
    	//rlong.
    	try {
    		rlong.getAndSet(100);	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	/*stringRedisTemplate.opsForValue().set("aaa","100");
    	Assert.assertEquals("100", stringRedisTemplate.opsForValue().get("aaa"));*/
    }
    public static void main(String[] args) {
    	Apptests apptests=new Apptests();
    	try {
			apptests.Distributed();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}

}
