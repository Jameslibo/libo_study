/**
 * @author Administrator 
 * @date 2019年7月10日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.stuady.design.strategy.PreEntry;
import com.stuady.design.strategy.PreEntryIDService;


/**  
 * <p>Title: MyApcationRun</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年7月10日  
 */
@Component // 被spring容器管理
@Order(1)
public class MyApcationRun implements ApplicationRunner {

	@Autowired
	@Qualifier("PreEntryIDServiceImpl")
	PreEntryIDService preEntryIDService;
	
	
	/* (non-Javadoc)
	 * <p>Title: run</p>
	 * <p>Description: </p>
	 * @param args
	 * @throws Exception
	 * @see org.springframework.boot.ApplicationRunner#run(org.springframework.boot.ApplicationArguments)
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		preEntryIDService.checks(PreEntry.builder().age(10).nameString("libo1").build());
	}

}
