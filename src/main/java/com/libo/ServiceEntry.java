/**
 * @author Administrator 
 * @date 2019年5月16日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.libo;

import static org.apache.commons.lang3.StringUtils.split;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lmax.disruptor.BusySpinWaitStrategy;
import com.lmax.disruptor.EventFactory;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;


/**  
 * <p>Title: Service</p>  
 * <p>Description: </p>  
 * @author libo  
 * @date 2019年5月16日  
 */
public class ServiceEntry  {
	
	@Autowired
	SortHandle sortHandle;
	public  RingBuffer<EventEntry> temp;
    
	private	final Disruptor<EventEntry> disruptor;
	ServiceEntry(){
		 int bufferSize=512; 
		 ExecutorService executor=Executors.newFixedThreadPool(8);
	      disruptor = new Disruptor<EventEntry>(new EventFactory<EventEntry>() {  
	            @Override  
	            public EventEntry newInstance() {  
	                return new EventEntry();  
	            }  
	        }, bufferSize, executor, ProducerType.SINGLE, new BusySpinWaitStrategy());  
	        
	}
	
   public  void  shutDown(){
	   
	   disruptor.shutdown();
	  
	  // System.out.println(  disruptor.getRingBuffer().getCursor());
	 
	//   disruptor.getRingBuffer().claimAndGetPreallocated(sequence)
   }	
   
   public  void  start() {
	
	   SortHandle.sort(disruptor);
	  // disruptor.handleEventsWith(new Handler1());
	   disruptor.setDefaultExceptionHandler(new MyExceptionHandle());
	   disruptor.start();
	   temp =  disruptor.getRingBuffer();
}

   public static  void main(String[] args) throws IOException  {
	      ObjectMapper MAPPER = new ObjectMapper();
	   HashedMap<String, Object> test=new HashedMap<String, Object>();
	   test.put("test1", "libo");
	   test.put("test2", Arrays.asList(1,2,3,4));
	   HashedMap<String, Object> test2 = null;
	   try {
		String string = MAPPER.writeValueAsString(test);
		System.out.println(string);
		
		test2=MAPPER.readValue(string, HashedMap.class);
		test2.keySet().forEach(key->{
			
			System.out.println("***"+key);
			
			
		});
		//test2.entrySet().stream().filter(s->s.getKey().equals("test2")).flatMap(Arrays::stream).forEach(System.out::println);//map(srv->srv.split("")).collect(Collectors.toList()).forEach(System.out::println);
		test2.entrySet().stream().filter(s->s.getKey().equals("test2")).filter(s->((List)s.getValue()).size()>2).forEach(s->System.out.println(s.getKey()+"****"+s.getValue()));
		
	} catch (JsonProcessingException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	   
	   
	   
	 /*  
	   ServiceEntry serviceEntry=new ServiceEntry();
	   serviceEntry.start();
		  System.out.println(  serviceEntry.disruptor.getRingBuffer().getCursor());
		  //serviceEntry.disruptor.setDefaultExceptionHandler(exceptionHandler);
	 
	   EventEventProducer producer=new EventEventProducer(serviceEntry.disruptor.getRingBuffer());
	   producer.onData("libo1-");
	   producer.onData("libo2-");
	
		 // new Thread(new EventProduerTask(serviceEntry.temp)).start();
		  SequenceBarrier sequenceBarrier=serviceEntry.temp.newBarrier();
		  sequenceBarrier.clearAlert();
	 // System.out.println( serviceEntry.temp.getBufferSize());
	 // System.out.println(  serviceEntry.disruptor.getRingBuffer().getCursor()+"  "+serviceEntry.disruptor.getRingBuffer().next());
	 // serviceEntry.disruptor.shutdown();
	  //System.out.println( serviceEntry.disruptor.shutdown;
	 // TimeUnit.SECONDS.sleep(3);
	 // System.out.println(" AAA"+serviceEntry.disruptor.getBarrierFor(handler).);
		  try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		  
		  serviceEntry.start();
		//  serviceEntry.disruptor.
		 // SortHandle.sort(serviceEntry.disruptor);
		   producer.onData("libo3-");
		   producer.onData("libo4-");
		 // serviceEntry.disruptor.shutdown();
	  

	*/
   }
   }
