package com.libo;

import com.lmax.disruptor.EventHandler;

public class Handler3 implements EventHandler<EventEntry> {
    @Override  
    public void onEvent(EventEntry event, long sequence,  boolean endOfBatch) throws Exception {  
     	
    	event.setPerEntry("handler3:");
    	System.out.println(event.getPerEntry());
    }  
}
