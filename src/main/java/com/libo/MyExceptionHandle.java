/**
 * @author Administrator 
 * @date 2019年5月28日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.libo;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.FatalExceptionHandler;


/**  
 * <p>Title: MyExceptionHandle</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年5月28日  
 */
public class MyExceptionHandle implements ExceptionHandler<Object>{
	
	 private static final Logger LOGGER = Logger.getLogger(FatalExceptionHandler.class.getName());
	 private final Logger logger;

	 
	 public MyExceptionHandle()
	    {
	        this.logger = LOGGER;
	    }

	/* (non-Javadoc)
	 * <p>Title: handleEventException</p>
	 * <p>Description: </p>
	 * @param ex
	 * @param sequence
	 * @param event
	 * @see com.lmax.disruptor.ExceptionHandler#handleEventException(java.lang.Throwable, long, java.lang.Object)
	 */
	@Override
	public void handleEventException(Throwable ex, long sequence, Object event) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Exception processing: " + sequence + " " + event, ex);
	}

	/* (non-Javadoc)
	 * <p>Title: handleOnStartException</p>
	 * <p>Description: </p>
	 * @param ex
	 * @see com.lmax.disruptor.ExceptionHandler#handleOnStartException(java.lang.Throwable)
	 */
	@Override
	public void handleOnStartException(Throwable ex) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Exception during onStart()", ex);
	}

	/* (non-Javadoc)
	 * <p>Title: handleOnShutdownException</p>
	 * <p>Description: </p>
	 * @param ex
	 * @see com.lmax.disruptor.ExceptionHandler#handleOnShutdownException(java.lang.Throwable)
	 */
	@Override
	public void handleOnShutdownException(Throwable ex) {
		// TODO Auto-generated method stub
		logger.log(Level.SEVERE, "Exception during onShutdown()", ex);
	}

	
	
	
}
