/**
 * @author Administrator 
 * @date 2019年5月16日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.libo;

import static com.google.common.collect.Sets.intersection;

import com.lmax.disruptor.SleepingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.EventHandlerGroup;


/**  
 * <p>Title: SortHandle</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年5月16日  
 */
public class SortHandle  implements Runnable{
	
   public  static  void	sort(Disruptor disruptor){
	  
	     //使用disruptor创建消费者组C1,C2  
      EventHandlerGroup<EventEntry> handlerGroup = 
      		disruptor.handleEventsWith(new Handler1(), new Handler2());
      //声明在C1,C2完事之后执行JMS消息发送操作 也就是流程走到C3 
      handlerGroup.then(new Handler3());
  }
   public  static  void	sort2(Disruptor disruptor){
		  
	     //使用disruptor创建消费者组C1,C2  
    EventHandlerGroup<EventEntry> handlerGroup = 
    		disruptor.handleEventsWith( new Handler2());
    //声明在C1,C2完事之后执行JMS消息发送操作 也就是流程走到C3 
    handlerGroup.then(new Handler3());
}

  public static void main(String s[]) throws InterruptedException{
	  
	  int i=0;
	  while(true){
		  
		  System.out.println("out:"+i++);
		 // Thread.currentThread().sleep(10000);
		  Thread.sleep(10000);
		  
	  }
  }
/* (non-Javadoc)
 * <p>Title: run</p>
 * <p>Description: </p>
 * @see java.lang.Runnable#run()
 */
@Override
public void run() {
	// TODO Auto-generated method stub
	  int i=0;
	  while(true){
		  
		  System.out.println("out:"+i++);
		 //  Thread.sleep(10000);
		 
		   
		  
	  }
}
   
}
