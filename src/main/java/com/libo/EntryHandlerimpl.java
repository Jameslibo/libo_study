/**
 * @author Administrator 
 * @date 2019年5月16日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.libo;

import com.lmax.disruptor.EventHandler;


/**  
 * <p>Title: EventHandler</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年5月16日  
 */
public class EntryHandlerimpl  implements EventHandler<EventEntry>{

	/* (non-Javadoc)
	 * <p>Title: onEvent</p>
	 * <p>Description: </p>
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @throws Exception
	 * @see com.lmax.disruptor.EventHandler#onEvent(java.lang.Object, long, boolean)
	 */
	@Override
	public void onEvent(EventEntry arg0, long sequence, boolean endOfBatch) throws Exception {
		// TODO Auto-generated method stub
		//业务处理逻辑
		 System.out.println("例如对预录入单号进行相应修改");  
		 arg0.setPerEntry("修改部分属性");
	}

}
