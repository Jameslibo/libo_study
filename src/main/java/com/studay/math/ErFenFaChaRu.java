package com.studay.math;

import org.springframework.context.support.StaticApplicationContext;

public class ErFenFaChaRu {
	
	public static int[] erfenFaChaRu(int a[]) {
		
		for (int i = 0; i < a.length; i++) {
            int temp = a[i];
            int left = 0;
            int right = i - 1;
            int mid = 0;
            while (left <= right) {
                mid = (left + right) / 2;
                if (temp < a[mid]) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
            for (int j = i - 1; j >= left; j--) {
                a[j + 1] = a[j];
            }
            if (left != i) {
                a[left] = temp;
            }
        }
        return a;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
