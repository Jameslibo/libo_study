package com.studay.math;

public class QuickSort {
	
	public static int divide(int a[],int start,int end){
		
		int base=a[end];
		//
		while(start<end){
			
			//**************从左边开始遍历 
			while(start<end&& a[start]<=base){
				start++;		
			//此循环结束后得到的start的值即大于基准值
			}
			//a[start]>base 交换
			
			if(start<end){
				int temp=a[end];
				a[end]=a[start];
				a[start]=temp;
				end--;
			}
			//***************************
			
			
			
			//*****************从右边开始遍历
			while(start<end&&a[end]>=base){
				end--;
				
			}
			if(start<end){
				int temp=a[start];
				a[start]=a[end];
				a[end]=temp;
				start++;
			}
			
			
			
		}
		
		
		
		return end;
	}
	
	
	public static void sort(int a[],int start,int end){
		
		if(start>end){
			return;
		}else{
			
			//如果不止一个元素继续划分两边递归排序下去
			int partition =divide(a,start,end);
			sort(a, start, partition-1);
			sort(a, partition+1, end);
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a[]={3,9,1,26,33,23,7,20,14};
		
		sort(a, 0, a.length-1);
		
		for(int w:a){
			System.out.println(w);
		}
			
	}

}
