package com.studay.poxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyHandler implements InvocationHandler{

	private Object target;
	
	public  MyHandler(Object target1){
		// TODO Auto-generated constructor stu
		
		this.target=target1;
	}
	
	// AbstractPlatfromTransationManager
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// TODO Auto-generated method stub
		//代理对象自己的业务
        //额外的业务
		//AbstractPlatformTransactionManager
		System.out.println("*************************加入了额外的处理******************");
		//日志记录
        //权限控制
        //事务控制
		//.........
		
		
		return method.invoke(target, args);
		
		
	}

}
