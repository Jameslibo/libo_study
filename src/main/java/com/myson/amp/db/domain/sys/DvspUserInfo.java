package com.myson.amp.db.domain.sys;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;

public class DvspUserInfo {
    private String id;

    private String loginName;

    private String password;

    private String name;

    private BigDecimal gender;

    private String email;

    private String address;

    private String mobile;

    private String orgId;

    private BigDecimal locked;

    private String creator;

    private Date createTime;

    private String modifier;

    private Date modifyTime;

    private String deptId;

	@Generated("SparkTools")
	private DvspUserInfo(Builder builder) {
		this.id = builder.id;
		this.loginName = builder.loginName;
		this.password = builder.password;
		this.name = builder.name;
		this.gender = builder.gender;
		this.email = builder.email;
		this.address = builder.address;
		this.mobile = builder.mobile;
		this.orgId = builder.orgId;
		this.locked = builder.locked;
		this.creator = builder.creator;
		this.createTime = builder.createTime;
		this.modifier = builder.modifier;
		this.modifyTime = builder.modifyTime;
		this.deptId = builder.deptId;
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName == null ? null : loginName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public BigDecimal getGender() {
        return gender;
    }

    public void setGender(BigDecimal gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public BigDecimal getLocked() {
        return locked;
    }

    public void setLocked(BigDecimal locked) {
        this.locked = locked;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId == null ? null : deptId.trim();
    }

	/**
	 * Creates builder to build {@link DvspUserInfo}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link DvspUserInfo}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String id;
		private String loginName;
		private String password;
		private String name;
		private BigDecimal gender;
		private String email;
		private String address;
		private String mobile;
		private String orgId;
		private BigDecimal locked;
		private String creator;
		private Date createTime;
		private String modifier;
		private Date modifyTime;
		private String deptId;

		private Builder() {
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Builder withLoginName(String loginName) {
			this.loginName = loginName;
			return this;
		}

		public Builder withPassword(String password) {
			this.password = password;
			return this;
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withGender(BigDecimal gender) {
			this.gender = gender;
			return this;
		}

		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder withAddress(String address) {
			this.address = address;
			return this;
		}

		public Builder withMobile(String mobile) {
			this.mobile = mobile;
			return this;
		}

		public Builder withOrgId(String orgId) {
			this.orgId = orgId;
			return this;
		}

		public Builder withLocked(BigDecimal locked) {
			this.locked = locked;
			return this;
		}

		public Builder withCreator(String creator) {
			this.creator = creator;
			return this;
		}

		public Builder withCreateTime(Date createTime) {
			this.createTime = createTime;
			return this;
		}

		public Builder withModifier(String modifier) {
			this.modifier = modifier;
			return this;
		}

		public Builder withModifyTime(Date modifyTime) {
			this.modifyTime = modifyTime;
			return this;
		}

		public Builder withDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}

		public DvspUserInfo build() {
			return new DvspUserInfo(this);
		}
	}
}