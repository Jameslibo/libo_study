package com.myson.amp.db.mapper.sys;

import com.myson.amp.db.domain.sys.DvspUserInfo;
import com.myson.amp.db.domain.sys.DvspUserInfoExample;
import java.util.List;


public interface DvspUserInfoMapper {
    long countByExample(DvspUserInfoExample example);

    int deleteByExample(DvspUserInfoExample example);

    int insert(DvspUserInfo record);

    int insertSelective(DvspUserInfo record);

    List<DvspUserInfo> selectByExample(DvspUserInfoExample example);

    //int updateByExampleSelective(@Param("record") DvspUserInfo record, @Param("example") DvspUserInfoExample example);

    //int updateByExample(@Param("record") DvspUserInfo record, @Param("example") DvspUserInfoExample example);
}