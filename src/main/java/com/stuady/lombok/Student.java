/**
 * @author Administrator 
 * @date 2019年5月22日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.lombok;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**  
 * <p>Title: Student</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年5月22日  
 */
@Getter
@Setter
@ToString
@Builder
public class Student {
	
	@Builder.ObtainVia(field = "fieldName")
	private String nameString;
	@Builder.Default
	private int age=10;
	
	
}
