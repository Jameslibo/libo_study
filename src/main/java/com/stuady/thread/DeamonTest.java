package com.stuady.thread;

public class DeamonTest {

	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName() + "start");
		
		ThreadDeamon t1 = new ThreadDeamon();
		ThreadDeamon t2 = new ThreadDeamon();
		
		t1.setName("守护线程1");
		//t2.setName("守护线程2");
		
		//t1.setDaemon(true);
		//t2.setDaemon(true);
		
		t1.start();
		//t2.start();
		
		System.out.println(Thread.currentThread().getName() + "end");
	}
}
