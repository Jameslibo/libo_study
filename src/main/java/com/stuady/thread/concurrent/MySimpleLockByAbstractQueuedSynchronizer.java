/**
 * @author Administrator 
 * @date 2019年4月17日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.thread.concurrent;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;


/**  
 * <p>Title: MySimpleLockByAbstractQueuedSynchronizer</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年4月17日  
 */
public class MySimpleLockByAbstractQueuedSynchronizer extends AbstractQueuedSynchronizer{

	 public void lock()  { acquire(1); }
	 public boolean tryLock()  { return tryAcquire(1); }
	 public void unlock(){ release(1); }
	 public boolean isLocked() { return isHeldExclusively(); }
	
	/**
	 * <p>Title: main</p>
	 * <p>Description: </p>
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MySimpleLockByAbstractQueuedSynchronizer simpleLockByAbstractQueuedSynchronizer=new MySimpleLockByAbstractQueuedSynchronizer();
		simpleLockByAbstractQueuedSynchronizer.lock();
		
		
	}

}
