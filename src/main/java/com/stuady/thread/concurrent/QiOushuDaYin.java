package com.stuady.thread.concurrent;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.context.support.StaticApplicationContext;


/**
 * 奇偶数交替打印
 * @author Administrator
 *
 */
public class QiOushuDaYin {
	  int i;
	  //private final static  
	  final  ReentrantLock reLock=new ReentrantLock();
	 Condition qi_Flage =reLock.newCondition();
	Condition ou_Flage =reLock.newCondition();
	 //偶数
	 boolean flageqi=true;
	 QiOushuDaYin(int i){
		 this.i=i;
		 if(i%2!=0){
			 flageqi= false;
		 }
	 }
	 
	 public void qi1() {
		
			while(i<100){
		try {
			reLock.lock();
			if(!flageqi){
				
				System.out.println("当前线程：" + Thread.currentThread().getName() + "奇数进入");
				System.out.println(i);
				i++;
				flageqi=true;
				 qi_Flage.await();
				//ou_Flage.signal();
				
				//qi_Flage.wait();
			}else {
				//qi_Flage.wait();
				//reLock.wait(200);
				System.out.println("奇数"+i);
			} 
	
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				// TODO: handle exception
				reLock.unlock();
			} }
	}
	
	 
	 public void ou1() {
		
			while(i<100){
				try {
					reLock.lock();
			if(flageqi){
				
				System.out.println("当前线程：" + Thread.currentThread().getName() + "偶数进入");
				System.out.println(i);
				i++;
				flageqi=false;
				qi_Flage.signal();
				//ou_Flage.await();
				
			}else {
				//qi_Flage.signal();
				//qi_Flage.signal();
				System.out.println("偶数"+i);
			}
		} finally {
			// TODO: handle exception
			reLock.unlock();
		} }
	
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	 final	QiOushuDaYin temp=new QiOushuDaYin(5);
	 
	 Thread thread1= new Thread(new Runnable() {
		
		public void run() {
			temp.ou1();
			
		}},"No2  偶数");
	 
		
	 
	 Thread thread2= new Thread(new Runnable() {
			
		public void run() {
			temp.qi1();
		}
	 },"No1  奇数");
	 
	
	 thread2.start();
	 thread1.start();
	}

	
	
}
