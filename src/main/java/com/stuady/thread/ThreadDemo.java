package com.stuady.thread;

public class ThreadDemo {

	public static void main(String[] args) throws InterruptedException {
		 SellTickets st = new SellTickets();
		 
		 Thread t1 = new Thread(st,"窗口1");
		 Thread t2 = new Thread(st,"窗口2");
		 Thread t3 = new Thread(st,"窗口3");
		// t1.setDaemon(true);
		
		
		// t1.setPriority(Thread.MAX_PRIORITY);
		 t1.start();
		 t2.start();
		 t3.start();
		 t1.join();
		// t1.interrupt();
	
		 
	}
}
