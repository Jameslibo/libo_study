package com.stuady.thread.pool;



import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;









import rx.Observable;
import rx.functions.Action1;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixThreadPoolKey;
import com.netflix.hystrix.HystrixThreadPoolProperties;

public class CommandUser extends HystrixCommand<String> {

private final static Logger LOGGER = LoggerFactory.getLogger(CommandUser.class);

private String userName;

public CommandUser(String userName) {

super(Setter.withGroupKey(
//服务分组
HystrixCommandGroupKey.Factory.asKey("UserGroup"))
//线程分组
.andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("UserPool"))
//线程池配置
.andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.Setter()
.withCoreSize(10)
.withKeepAliveTimeMinutes(5)
.withMaxQueueSize(10)
.withQueueSizeRejectionThreshold(10000))

//线程池隔离
.andCommandPropertiesDefaults(
HystrixCommandProperties.Setter()
.withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.THREAD))
);


this.userName = userName;
}


@Override
public String run() throws Exception {
LOGGER.info("userName=[{}]", userName);
TimeUnit.MILLISECONDS.sleep(100);
return "userName=" + userName;
}

public static void main(String[] args) throws Exception {
    CommandOrder commandPhone = new CommandOrder("手机");
    CommandOrder command = new CommandOrder("电视");
    CommandOrder commandlibo = new CommandOrder("libo");
    CommandOrder command2 = new CommandOrder("电视2");
    CommandOrder commandlibo2 = new CommandOrder("libo2");
    
    
   
    
    
    String execute2 = commandlibo.execute();
    LOGGER.info("execute=[{}]", execute2);

    
    LOGGER.info("execute=[{}]", command2.execute());
    
 //阻塞方式执行
String execute = commandPhone.execute();
LOGGER.info("execute=[{}]", execute);

//异步非阻塞方式
Future<String> queue = command.queue();
String value = queue.get(200, TimeUnit.MILLISECONDS);
LOGGER.info("“value=[{}]", value);



CommandUser commandUser = new CommandUser("张三");
String name = commandUser.execute();
LOGGER.info("name=[{}]", name);




Observable<String> fs  =commandlibo2.observe();

fs.subscribe(new Action1<String>() {

    public void call(String result) {
        //执行结果处理,result 为HelloWorldCommand返回的结果
        //用户对结果做二次处理.
        System.out.println("result   :"+result);
    }
});
}





}