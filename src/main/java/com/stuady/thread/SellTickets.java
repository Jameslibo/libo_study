package com.stuady.thread;

public class SellTickets implements Runnable{

	private int tickets = 100;
	
	public void run() {
		while(true){
			synchronized(this){
				if(tickets > 0){
					System.out.println(Thread.currentThread().getName() 
							+ "正在销售第" + (tickets --) + "张票");
				}
			}
			
		}
		
	}

}
