/**
 * @author Administrator 
 * @date 2019年6月24日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.guava;

import com.google.common.base.Preconditions;


/**  
 * <p>Title: PreconditionsTest</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月24日  
 */
public class PreconditionsTest {

 
	
	
	/**
	 * 确保开始和结束指定数组，列表或字符串大小有效的位置，并按照顺序
	 * @create 2017-8-18 下午5:26:23
	 */
	private static void testCheckPositionIndexes() {
		/**
		 * 确保开始和结束指定数组，列表或字符串大小有效的位置，并按照顺序
		 * 这个方法只要表达的意思是
		 * start作为开始位置
		 * end作为结束位置
		 * size作为数组或者列表或者字符串的长度
		 * 
		 * 开始的位置 (start) <= 结束的位置(end) <= 总长度(size)
		 * 如果有一个不满足则抛出异常：IndexOutOfBoundsException
		 */
//		Preconditions.checkPositionIndexes(start, end, size)
	}
 
 
 
 
	/**
	 * 确保索引指定一个数组，列表或尺寸大小的字符串有效的元素。
	 * 异常信息：IndexOutOfBoundsException
	 * @create 2017-8-18 下午5:17:27
	 */
	private static void textCheckPositionIndex() {
 
		String array = "aaa";
		int index = 5;
		
		try {
//			Preconditions.checkElementIndex(index,array.length);
			int result = Preconditions.checkElementIndex(index,array.length(),"大于数组的长度啦");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		/**
		 * Exception in thread "main" java.lang.IndexOutOfBoundsException: 大于数组的长度啦 (5) must be less than size (3)
				at com.google.common.base.Preconditions.checkElementIndex(Preconditions.java:310)
				at com.lwl.guava.Preconditions.UserPreconditions.testCheckElementIndex(UserPreconditions.java:35)
				at com.lwl.guava.Preconditions.UserPreconditions.main(UserPreconditions.java:25)
		 */
	}
	
	/**
	 * 确保索引指定一个数组，列表或尺寸大小的字符串有效的元素。
	 * 异常信息：IndexOutOfBoundsException
	 * @create 2017-8-18 下午5:17:27
	 */
	private static void testCheckElementIndex() {
		
		int[] array = {1,2,3};
		int index = 2;
		
		try {
//			Preconditions.checkElementIndex(index,array.length);
			int result = Preconditions.checkElementIndex(index,array.length,"大于数组的长度啦");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		/**
		 * Exception in thread "main" java.lang.IndexOutOfBoundsException: 大于数组的长度啦 (5) must be less than size (3)
				at com.google.common.base.Preconditions.checkElementIndex(Preconditions.java:310)
				at com.lwl.guava.Preconditions.UserPreconditions.testCheckElementIndex(UserPreconditions.java:35)
				at com.lwl.guava.Preconditions.UserPreconditions.main(UserPreconditions.java:25)
		 */
	}
 
 
 
 
	/**
	 * 
	 * 用来检查对象的某些状态。
	 * 确保涉及调用实例的状态，但不涉及任何参数来调用方法表达式的真相。
	 * 检查失败时抛出的异常：IllegalStateException
	 * @create 2017-8-18 下午5:07:46
	 */
	private static void testCheckState() {
		//checkState(boolean expression)
//		Preconditions.checkState(2 > 5);
		//checkState(boolean expression, String errorMessageTemplate)
//		Preconditions.checkState(2 > 5,"2不可能大于5");
		try {
			//checkState(boolean expression, String errorMessageTemplate, Object... errorMessageArgs)
			Preconditions.checkState(2 > 5,"2不可能大于5",1,2,3,4);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		/**
		 * 错误信息：
		 * java.lang.IllegalStateException: 2不可能大于5 [1, 2, 3, 4]
				at com.google.common.base.Preconditions.checkState(Preconditions.java:199)
				at com.lwl.guava.Preconditions.UserPreconditions.testCheckState(UserPreconditions.java:40)
				at com.lwl.guava.Preconditions.UserPreconditions.main(UserPreconditions.java:23)
			2不可能大于5 [1, 2, 3, 4]
		 */
		
	}
 
 
 
 
 
	/**
	 * 检查value是否为null，该方法直接返回value，因此可以内嵌使用checkNotNull。
	 * 检查失败时抛出的异常：NullPointerException
	 * @create 2017-8-18 下午5:00:27
	 */
	private static void testCheckNotNull() {
 
		Integer first = null;
		//确保对象引用作为参数传递给调用方法不为空。
//		Integer result1 = Preconditions.checkNotNull(first);
		//确保对象引用作为参数传递给调用方法不为空。
//		Integer result2 = Preconditions.checkNotNull(first,"是空值哦");
		//checkNotNull(T reference, String errorMessageTemplate, Object... errorMessageArgs)
		//第三个参数是可变参数
		Integer result3 = Preconditions.checkNotNull(first,"是空值哦",12,123,123);
		System.out.println(result3);
		/**
		 * 异常显示
		 * Exception in thread "main" java.lang.NullPointerException: 是空值哦 [12, 123, 123]
				at com.google.common.base.Preconditions.checkNotNull(Preconditions.java:251)
				at com.lwl.guava.Preconditions.UserPreconditions.testCheckNotNull(UserPreconditions.java:39)
				at com.lwl.guava.Preconditions.UserPreconditions.main(UserPreconditions.java:18)
		 */
	}

	
	/**
	 * 检查boolean是否为true，用来检查传递给方法的参数。
	 * 检查失败时抛出的异常：IllegalArgumentException
	 * @create 2017-8-18 下午4:56:28
	 */
	public static void testCheckArgument(){
		//没有额外参数：抛出的异常中没有错误消息；
		try {
			Preconditions.checkArgument(5 > 10);
		} catch (IllegalArgumentException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		//确保涉及的一个或多个参数来调用方法表达式的真相。checkArgument(boolean expression, Object errorMessage)
		try {
			Preconditions.checkArgument(5 > 10,"5肯定小于10 啦");
		} catch (IllegalArgumentException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		//确保涉及的一个或多个参数来调用方法表达式的真相。 checkArgument(boolean expression, 
		//				String errorMessageTemplate, Object... errorMessageArgs)
		try {
			Preconditions.checkArgument(11 > 10,"5肯定小于10 啦",4,32,423);
		} catch (IllegalArgumentException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
			System.out.println(e.getLocalizedMessage().indexOf(1));
		}
	}
	

	/**
	 * Preconditions提供静态方法来检查方法或构造函数，被调用是否给定适当的参数。
	 * 它检查的先决条件。其方法失败抛出IllegalArgumentException。
	 * @param args
	 * @author 
	 * @create 2017-8-18 下午4:44:46
	 */
	public static void main(String[] args) {
 
		//检查boolean是否为true，用来检查传递给方法的参数。
//		testCheckArgument();
		
		//检查value是否为null，该方法直接返回value，因此可以内嵌使用checkNotNull。
	testCheckNotNull();
		
		//用来检查对象的某些状态。
//		testCheckState();
		
		//确保索引指定一个数组，列表或尺寸大小的字符串有效的元素。
		testCheckElementIndex();
		
		//确保索引指定一个数组，列表或尺寸大小的字符串有效的元素。
//		textCheckPositionIndex();
		
		//确保开始和结束指定数组，列表或字符串大小有效的位置，并按照顺序
//		testCheckPositionIndexes();
		
	}
}
