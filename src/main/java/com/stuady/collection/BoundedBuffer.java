package com.stuady.collection;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.Generated;

class BoundedBuffer {
	 Lock lock = new ReentrantLock();
	 Condition notFull = lock.newCondition();
	 Condition notEmpty = lock.newCondition();

	 Object[] items = new Object[100];
	int putptr, takeptr, count;

	@Generated("SparkTools")
	private BoundedBuffer(Builder builder) {
		this.lock = builder.lock;
		this.notFull = builder.notFull;
		this.notEmpty = builder.notEmpty;
		this.items = builder.items;
		this.putptr = builder.putptr;
		this.takeptr = builder.takeptr;
		this.count = builder.count;
	}

	public void put(Object x) throws InterruptedException {
		lock.lock();
		try {
			while (count == items.length)
				notFull.await();
			items[putptr] = x;
			if (++putptr == items.length) putptr = 0;
			++count;
			notEmpty.signal();
		} finally {
			lock.unlock();
		}
	}

	public Object take() throws InterruptedException {
		lock.lock();
		try {
			while (count == 0)
				notEmpty.await();
			Object x = items[takeptr];
			if (++takeptr == items.length) takeptr = 0;
			--count;
			notFull.signal();
			return x;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Creates builder to build {@link BoundedBuffer}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link BoundedBuffer}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Lock lock;
		private Condition notFull;
		private Condition notEmpty;
		private Object[] items;
		private int putptr;
		private int takeptr;
		private int count;

		private Builder() {
		}

		public Builder withLock(Lock lock) {
			this.lock = lock;
			return this;
		}

		public Builder withNotFull(Condition notFull) {
			this.notFull = notFull;
			return this;
		}

		public Builder withNotEmpty(Condition notEmpty) {
			this.notEmpty = notEmpty;
			return this;
		}

		public Builder withItems(Object[] items) {
			this.items = items;
			return this;
		}

		public Builder withPutptr(int putptr) {
			this.putptr = putptr;
			return this;
		}

		public Builder withTakeptr(int takeptr) {
			this.takeptr = takeptr;
			return this;
		}

		public Builder withCount(int count) {
			this.count = count;
			return this;
		}

		public BoundedBuffer build() {
			return new BoundedBuffer(this);
		}
	}

	public static void main(String[] args) {

		//BoundedBuffer boundedBuffer = new BoundedBuffer();
		// boundedBuffer.
		BoundedBuffer.builder();

	}
}
