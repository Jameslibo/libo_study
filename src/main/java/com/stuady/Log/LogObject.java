/**
 * @author Administrator 
 * @date 2019年6月14日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.Log;

import java.util.Iterator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.slf4j.Marker;


/**  
 * <p>Title: LogObject</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月14日  
 */
@Getter
@Setter
@ToString
public class LogObject implements Marker{
	
	String nameString;
	String ipString;
	/* (non-Javadoc)
	 * <p>Title: getName</p>
	 * <p>Description: </p>
	 * @return
	 * @see org.slf4j.Marker#getName()
	 */
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return nameString+ipString;
	}
	/* (non-Javadoc)
	 * <p>Title: add</p>
	 * <p>Description: </p>
	 * @param reference
	 * @see org.slf4j.Marker#add(org.slf4j.Marker)
	 */
	@Override
	public void add(Marker reference) {
		// TODO Auto-generated method stub
		
	}
	/* (non-Javadoc)
	 * <p>Title: remove</p>
	 * <p>Description: </p>
	 * @param reference
	 * @return
	 * @see org.slf4j.Marker#remove(org.slf4j.Marker)
	 */
	@Override
	public boolean remove(Marker reference) {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * <p>Title: hasChildren</p>
	 * <p>Description: </p>
	 * @return
	 * @deprecated
	 * @see org.slf4j.Marker#hasChildren()
	 */
	@Override
	public boolean hasChildren() {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * <p>Title: hasReferences</p>
	 * <p>Description: </p>
	 * @return
	 * @see org.slf4j.Marker#hasReferences()
	 */
	@Override
	public boolean hasReferences() {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * <p>Title: iterator</p>
	 * <p>Description: </p>
	 * @return
	 * @see org.slf4j.Marker#iterator()
	 */
	@Override
	public Iterator<Marker> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * <p>Title: contains</p>
	 * <p>Description: </p>
	 * @param other
	 * @return
	 * @see org.slf4j.Marker#contains(org.slf4j.Marker)
	 */
	@Override
	public boolean contains(Marker other) {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * <p>Title: contains</p>
	 * <p>Description: </p>
	 * @param name
	 * @return
	 * @see org.slf4j.Marker#contains(java.lang.String)
	 */
	@Override
	public boolean contains(String name) {
		// TODO Auto-generated method stub
		return false;
	}

}
