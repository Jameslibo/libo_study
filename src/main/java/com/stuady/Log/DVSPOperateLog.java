package com.stuady.Log;

import java.util.Date;
import java.util.Iterator;

import javax.annotation.Generated;

import org.slf4j.Marker;

public class DVSPOperateLog implements Marker {
	private Long id;

	private String operaterid;

	private String operatername;

	private String resourcename;

	private String resourceid;

	private String action;

	private Date time;

	private String content;
	
	private String ip;

	@Generated("SparkTools")
	private DVSPOperateLog(Builder builder) {
		this.id = builder.id;
		this.operaterid = builder.operaterid;
		this.operatername = builder.operatername;
		this.resourcename = builder.resourcename;
		this.resourceid = builder.resourceid;
		this.action = builder.action;
		this.time = builder.time;
		this.content = builder.content;
		this.ip = builder.ip;
	}
	

	public DVSPOperateLog() {
		super();

	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOperaterid() {
		return operaterid;
	}
	
	public void setIp(String ip) {
		this.ip = ip == null ? null : ip.trim();
	}

	public String getIp() {
		return ip;
	}

	public void setOperaterid(String operaterid) {
		this.operaterid = operaterid == null ? null : operaterid.trim();
	}

	public String getOperatername() {
		return operatername;
	}

	public void setOperatername(String operatername) {
		this.operatername = operatername == null ? null : operatername.trim();
	}

	public String getResourcename() {
		return resourcename;
	}

	public void setResourcename(String resourcename) {
		this.resourcename = resourcename == null ? null : resourcename.trim();
	}

	public String getResourceid() {
		return resourceid;
	}

	public void setResourceid(String resourceid) {
		this.resourceid = resourceid == null ? null : resourceid.trim();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action == null ? null : action.trim();
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	/**
	 * Creates builder to build {@link DVSPOperateLog}.
	 * 
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link DVSPOperateLog}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String operaterid;
		private String operatername;
		private String resourcename;
		private String resourceid;
		private String action;
		private Date time;
		private String content;
		private String ip;
		private Builder() {
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIp(String ip) {
			this.ip = ip;
			return this;
		}
		public Builder withOperaterid(String operaterid) {
			this.operaterid = operaterid;
			return this;
		}

		public Builder withOperatername(String operatername) {
			this.operatername = operatername;
			return this;
		}

		public Builder withResourcename(String resourcename) {
			this.resourcename = resourcename;
			return this;
		}

		public Builder withResourceid(String resourceid) {
			this.resourceid = resourceid;
			return this;
		}

		public Builder withAction(String action) {
			this.action = action;
			return this;
		}

		public Builder withTime(Date time) {
			this.time = time;
			return this;
		}

		public Builder withContent(String content) {
			this.content = content;
			return this;
		}

		public DVSPOperateLog build() {
			return new DVSPOperateLog(this);
		}

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.operatername+"  "+this.content;
	}

	@Override
	public void add(Marker reference) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean remove(Marker reference) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasChildren() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasReferences() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<Marker> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean contains(Marker other) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(String name) {
		// TODO Auto-generated method stub
		return false;
	}

}