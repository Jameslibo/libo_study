/**
 * @author Administrator 
 * @date 2019年6月15日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.design.strategy.biz;

import org.springframework.stereotype.Service;

import com.stuady.design.strategy.AbstractCheck;
import com.stuady.design.strategy.PreEntry;
import com.stuady.design.strategy.annotation.CheckHandle;


/**  
 * <p>Title: FristCheck</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月15日  
 */
@CheckHandle(value="frist",batch=1,ifconcurrent=0)
@Service
public class FristCheck extends AbstractCheck {

	/* (non-Javadoc)
	 * <p>Title: check</p>
	 * <p>Description: </p>
	 * @param dto
	 * @return
	 * @see com.stuady.design.strategy.AbstractCheck#check(com.stuady.design.strategy.PreEntry)
	 */
	@Override
	public String check(PreEntry dto) {
		// TODO Auto-generated method stub
		return "frist ok";
	}
	
	public void writeData(){
		
		
		System.out.println("写数据完成 frist");
	}

	/* (non-Javadoc)
	 * <p>Title: read</p>
	 * <p>Description: </p>
	 * @see com.stuady.design.strategy.AbstractCheck#read()
	 */
	@Override
	public void read() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * <p>Title: write</p>
	 * <p>Description: </p>
	 * @param write
	 * @see com.stuady.design.strategy.AbstractCheck#write(java.lang.Object)
	 */
	@Override
	public void write(Object write) {
		// TODO Auto-generated method stub
		
	}

}
