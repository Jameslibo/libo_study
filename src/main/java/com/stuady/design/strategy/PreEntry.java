/**
 * @author Administrator 
 * @date 2019年6月15日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.design.strategy;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**  
 * <p>Title: PreEntry</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月15日  
 */
@Builder
@Setter
@Getter
public class PreEntry {
	
	private String nameString;
	private int age;

}
