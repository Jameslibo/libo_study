/**
 * @author Administrator 
 * @date 2019年6月15日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.design.strategy;


/**  
 * <p>Title: AbstractCheck</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月15日  
 */
public abstract class AbstractCheck {
	
	abstract public void read();
	abstract public String check(PreEntry dto);
	abstract public void write(Object write);
	

}
