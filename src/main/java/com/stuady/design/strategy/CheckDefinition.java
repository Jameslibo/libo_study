/**
 * @author Administrator 
 * @date 2019年6月16日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.design.strategy;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**  
 * <p>Title: BatchDescription</p>  
 * <p>Description: check 服务描述对象 </p>  
 * @author Administrator  
 * @date 2019年6月16日  
 */

public class CheckDefinition {
	
	
	public String getNameString() {
		return nameString;
	}
	
	public void setNameString(String nameString) {
		this.nameString = nameString;
	}
	
	public Integer getBatch() {
		return batch;
	}
	
	public void setBatch(Integer batch) {
		this.batch = batch;
	}
	
	public int getIfconcurrent() {
		return ifconcurrent;
	}
	
	public void setIfconcurrent(int ifconcurrent) {
		this.ifconcurrent = ifconcurrent;
	}
	
	public Class getClass1() {
		return class1;
	}
	
	public void setClass1(Class class1) {
		this.class1 = class1;
	}
	public CheckDefinition(String name,int batch,int ifconcurrent,Class class1){
		this.nameString=name;
		this.batch=batch;
		this.ifconcurrent=ifconcurrent;
		this.class1=class1;
		
	}
	
	private String nameString;
	private Integer batch;
	private int ifconcurrent;
	private Class class1 ;

}
