package com.stuady.design.strategy.service;

import java.util.Arrays;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import com.cipher.handler_demo.util.ClassScaner;
import com.google.common.collect.Maps;
import com.stuady.design.strategy.CheckDefinition;
import com.stuady.design.strategy.annotation.CheckHandle;

@Component
@SuppressWarnings("unchecked")
@Slf4j
public class HandlerProcessor implements BeanFactoryPostProcessor {

    private static final String HANDLER_PACKAGE = "com.stuady.design.strategy.biz";

    /**
     * 扫描@CheckHandle，初始化PreEntryCheckContent，将其注册到spring容器
     *
     * @param beanFactory bean工厂
     * @see HandlerType
     * @see PreEntryCheckContent
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        Map<String, CheckDefinition> handlerMap =Maps.newHashMap();
        
        ClassScaner.scan(HANDLER_PACKAGE, CheckHandle.class).forEach(clazz -> {
            String name = clazz.getAnnotation(CheckHandle.class).value();
            int batch = clazz.getAnnotation(CheckHandle.class).batch();
            int ifconcurrent = clazz.getAnnotation(CheckHandle.class).ifconcurrent();
            handlerMap.put(name,new CheckDefinition(name,batch,ifconcurrent,clazz));
        });
        PreEntryCheckContent context = new PreEntryCheckContent(handlerMap);
        beanFactory.registerSingleton(PreEntryCheckContent.class.getName(), context);
    }

}
