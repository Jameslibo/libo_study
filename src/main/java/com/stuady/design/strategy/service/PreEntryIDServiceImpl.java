/**
 * @author Administrator 
 * @date 2019年6月15日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.design.strategy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cipher.handler_demo.util.BeanTool;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.stuady.design.strategy.AbstractCheck;
import com.stuady.design.strategy.CheckDefinition;
import com.stuady.design.strategy.PreEntry;
import com.stuady.design.strategy.PreEntryIDService;



/**  
 * <p>Title: PreEntryIDServiceImpl</p>  
 * <p>Description: </p>  
 * @author Administrator  
 * @date 2019年6月15日  
 */
@Service("PreEntryIDServiceImpl")
public class PreEntryIDServiceImpl implements PreEntryIDService {

	
	
	//private Vector<E>  temp;   
	
      
	
	private Map<Integer,List<CheckDefinition> > checkgroup;
	

	ExecutorService executorService;
	
	 PreEntryIDServiceImpl(){
		ThreadFactory namefactory=new ThreadFactoryBuilder().setNameFormat("Shut--chechk-pool-%d").build();
		this.executorService=new ThreadPoolExecutor(4,8,0L,TimeUnit.MILLISECONDS,new LinkedBlockingQueue(1024),namefactory,new AbortPolicy());	                         
	 }
	
	 
	@Autowired
	private PreEntryCheckContent precontent; 
	/* 
	 * <p>Title: checks</p>
	 * <p>Description: </p>
	 * @param peEntry
	 * @return
	 * @see com.stuady.design.strategy.PreEntryIDService#checks(com.stuady.design.strategy.PreEntry)
	 */
	@Override
	public String checks(PreEntry peEntry) {
		// TODO Auto-generated method stub
		//precontent.getHandlerMap().keySet().stream().map(d->d.getBatch()).
	
		System.out.println("Start process perEntry");
		//AbstractCheck check= precontent.getInstance(type);
		HashMap test=batchProcessor(peEntry);
		return null;
	}
	

	/**
	 * 
	 * <p>Title: batchProcessor</p>
	 * <p>Description: </p>
	 * @param batch  批次号
	 * @param entitylist 处理服务list
	 */
    private HashMap batchProcessor(PreEntry peEntry){
    	
        HashMap testHashtable= new HashMap();
    	getGroup().forEach((k,v)->{
    		System.out.println("Batch="+k);
    		List listtemp=new ArrayList();
    		
		v.forEach(Item->{
	
			listtemp.add( entityProcessor(Item,peEntry)) ;
			
		});
			
		testHashtable.put(k, listtemp);
		});
		 return testHashtable;
    	
    	
    }
    
    /**
     * 单体处理
     * <p>Title: entityProcessor</p>
     * <p>Description: </p>
     */
    private Future<String> entityProcessor(CheckDefinition checkdefinition,PreEntry peEntry){
    	
    	AbstractCheck temp=(AbstractCheck) BeanTool.getBean(checkdefinition.getClass1());
    	
    	return  (Future<String>) this.executorService.submit(
    			new Runnable (){
    				public void run(){
    				
    				  System.out.println(temp.check(peEntry));	
    				}
    				
    			}
    			);
    
    }
    
    
    /**
     * 获取批次分组列表
     * <p>Title: getGroup</p>
     * <p>Description: </p>
     * @return
     */
    
    Map<Integer,List<CheckDefinition> > getGroup(){
    	try{
    		
    	   return Preconditions.checkNotNull(this.checkgroup);
    	
    	}catch(NullPointerException e){
    		
    		this.checkgroup= precontent.getHandlerMap().values().stream().collect(Collectors.groupingBy(CheckDefinition::getBatch));
    		return checkgroup; 
    	}
    }

}
