/**
 * @author Administrator
 * @date 2019年6月15日
 * @version 1.0
 *          <p>
 *          Company: https://www.anchnet.com
 *          </p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.stuady.design.strategy.service;

import java.util.Map;

import lombok.Getter;

import com.cipher.handler_demo.util.BeanTool;
import com.stuady.design.strategy.AbstractCheck;
import com.stuady.design.strategy.CheckDefinition;

/**
 * <p>
 * Title: PreEntryCheckContent
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author Administrator
 * @date 2019年6月15日
 */
@Getter
public class PreEntryCheckContent {

    private Map<String, CheckDefinition> handlerMap;

    public PreEntryCheckContent(Map<String, CheckDefinition> handlerMap) {
        this.handlerMap = handlerMap;
    }

    public AbstractCheck getInstance(String name) {
        Class clazz = handlerMap.get(name).getClass();
        if (clazz == null) {
            throw new IllegalArgumentException("not found handler for type: " + name);
        }
        return (AbstractCheck)BeanTool.getBean(clazz);
    }

}
