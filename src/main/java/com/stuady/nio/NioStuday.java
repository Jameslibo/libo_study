package com.stuady.nio;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class NioStuday {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// 
		PrintWriter pwrite;

		ByteBuffer b=ByteBuffer.allocate(15);
		ByteBuffer.allocateDirect(15);
		System.out.println("*1  limit="+b.limit()+" capacity="+b.capacity()+" position="+b.position()
				);
		
		for(int i=0;i<10;i++){
			b.put((byte) i);
		}
		System.out.println("*2  limit="+b.limit()+" capacity="+b.capacity()+" position="+b.position()
				);
		
		for(int i=5;i<10;i++){
			System.out.println(b.get(i));
		}
		
		System.out.println("*3  limit="+b.limit()+" capacity="+b.capacity()+" position="+b.position()
				);
		b.flip();

		System.out.println("*4  limit="+b.limit()+" capacity="+b.capacity()+" position="+b.position()
				);
		
		//file map in memory
		try {
			RandomAccessFile raf=new RandomAccessFile("d://libo.txt","rw");
			
			FileChannel fchannle=raf.getChannel();
			MappedByteBuffer mapbuffer=fchannle.map(FileChannel.MapMode.READ_WRITE, 0,raf.length() );
	        while(mapbuffer.hasRemaining()){
	        	System.out.println((char)mapbuffer.get());
	        }
	        mapbuffer.put(0,(byte)98);
	        raf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
