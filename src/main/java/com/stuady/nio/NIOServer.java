package com.stuady.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {

	private int blocksize=4096;
	private ByteBuffer sendbuffer=	ByteBuffer.allocate(blocksize);
	private ByteBuffer receiveBuffer= ByteBuffer.allocate(blocksize);
	
	private Selector selector;
	
	public NIOServer(int port) throws IOException{
		
		//open server link
		ServerSocketChannel serversocketchannel=ServerSocketChannel.open();
		//服务器配置为非阻塞
		serversocketchannel.configureBlocking(false);
		ServerSocket serversocket = null;
		//检索此通道关联的套接字
		if(serversocketchannel.isOpen()){
			 serversocket=serversocketchannel.socket();
		}
		//服务绑定
		serversocket.bind(new InetSocketAddress(port));
		//通过OPEN方法获取selector
		selector=Selector.open();
		
		//注册选择器
		serversocketchannel.register(selector, SelectionKey.OP_ACCEPT);
		
		System.out.println("Server is start-------");
		
	}
	
	private void listen() throws IOException{
		
		while(true){
			// ×阻塞× 选择一组键，其相应的通道已为 I/O 操作准备就绪  
			selector.select();
			Set<SelectionKey> selectorkeys=selector.selectedKeys();
			
			Iterator<SelectionKey> iterator=selectorkeys.iterator();
			while(iterator.hasNext()){
				SelectionKey selectorkey=iterator.next();
				iterator.remove();
				handleKey(selectorkey);
			}
			
		}
	}
	
	private void handleKey(SelectionKey selectionkey) throws IOException{

		//接受请求
		ServerSocketChannel serversocketchannel=null;
		SocketChannel socketchannelCilent=null;
		String receiveText;
		String sendText;
		int count=0;
		//测试通道是否已经准备好
		if(selectionkey.isAcceptable()){
			
			//返回为之创建此建的通到
			serversocketchannel=(ServerSocketChannel)selectionkey.channel();
			//接受到此套接字的连接
			//此方法返回的套接字如果有则为阻塞
			socketchannelCilent=serversocketchannel.accept();
			//配置为非阻塞
			socketchannelCilent.configureBlocking(false);
			//注册到selector等待连接
			socketchannelCilent.register(selector, SelectionKey.OP_READ);
			
			
		}else if(selectionkey.isReadable()){
			
			//返回为之创建此建的通道
			socketchannelCilent = (SocketChannel) selectionkey.channel();
			//将缓冲区清空以备下次读取
			receiveBuffer.clear();
            //读取服务器发送来的数据到缓冲区中
            count = socketchannelCilent.read(receiveBuffer);
            if(count>0){
            	receiveText = new String( receiveBuffer.array(),0,count);
                System.out.println("服务器端接受客户端数据--:"+receiveText);
               
                //
                socketchannelCilent.register(selector, SelectionKey.OP_WRITE);
            	
            }
			
		}else if(selectionkey.isWritable()){
			
			//将缓冲区清空以备下次写入
            sendbuffer.clear();
            // 返回为之创建此键的通道。
            socketchannelCilent = (SocketChannel) selectionkey.channel();
            sendText="message from server--";
            //向缓冲区中输入数据
            sendbuffer.put(sendText.getBytes());
            //将缓冲区各标志复位,因为向里面put了数据标志被改变要想从中读取数据发向服务器,就要复位
            sendbuffer.flip();
            //输出到通道
            socketchannelCilent.write(sendbuffer);
            System.out.println("服务器端向客户端发送数据--："+sendText);
            socketchannelCilent.register(selector, SelectionKey.OP_READ);
            
		}
		
	}
	
	  public static void main(String[] args) throws IOException {
	        int port = 22333;
	        NIOServer server = new NIOServer(port);
	        server.listen();
	    }
	
}
