package com.stuady.nio.reactor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

final class Handler implements Runnable {
    final SocketChannel socket;
    final SelectionKey sk;
   // ByteBuffer input = ByteBuffer.allocate(MAXIN);
    ByteBuffer input = ByteBuffer.allocate(1024);
   // ByteBuffer output = ByteBuffer.allocate(MAXOUT);
    ByteBuffer output = ByteBuffer.allocate(1024);
    static final int READING = 0, SENDING = 1;
    int state = READING;

    Handler(Selector sel, SocketChannel c) throws IOException {
        socket = c;
        c.configureBlocking(false);
        // Optionally try first read now
        sk = socket.register(sel, 0);
        sk.attach(this);
        sk.interestOps(SelectionKey.OP_READ);
        sel.wakeup();
    }

    boolean inputIsComplete() {
		return false;
        /* ... */ }

    boolean outputIsComplete() {
		return false;
        /* ... */ }

    void process() {
    	
    	
        /* ... */ }
    
    
    // class Handler continued
    //具体的请求处理，可能是读事件、写事件等
    public void run() {
        try {
            if (state == READING)
                read();
            else if (state == SENDING)
                send();
        } catch (IOException ex) {
            /* ... */ }
    }

    void read() throws IOException {
        int count=socket.read(input);
        
        System.out.println(new String(input.array(),0,count));
        
        if (inputIsComplete()) {
            process();
            state = SENDING;
            // Normally also do first write now
            sk.interestOps(SelectionKey.OP_WRITE);
        }
    }

    void send() throws IOException {
    	 int count=  socket.write(output);
        System.out.println(new String(output.array(),0,count));
        if (outputIsComplete())
            sk.cancel();
    }
    
}