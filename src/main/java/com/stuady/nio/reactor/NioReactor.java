package com.stuady.nio.reactor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NioReactor implements Runnable{
	
	
	private Selector selector;
	final ServerSocketChannel serversocketchannel;
	public NioReactor(int port) throws IOException{
		
		//open server link
		 serversocketchannel=ServerSocketChannel.open();
		//服务器配置为非阻塞
		serversocketchannel.configureBlocking(false);
		ServerSocket serversocket = null;
		//检索此通道关联的套接字
		if(serversocketchannel.isOpen()){
			 serversocket=serversocketchannel.socket();
		}
		//服务绑定
		serversocket.bind(new InetSocketAddress(port));
		//通过OPEN方法获取selector
		selector=Selector.open();
		
		//注册选择器
		SelectionKey sk=serversocketchannel.register(selector, SelectionKey.OP_ACCEPT);
		 sk.attach(new Acceptor());
		System.out.println("Server is start-------");
		
	}

	 // class Reactor continued
    //无限循环等待网络请求的到来
    //其中selector.select();会阻塞直到有绑定到selector的请求类型对应的请求到来，一旦收到事件，处理分发到对应的handler，并将这个事件移除
    public void run() { // normally in a new Thread
        try {
            while (!Thread.interrupted()) {
                selector.select();
                Set selected = selector.selectedKeys();
                Iterator it = selected.iterator();
                while (it.hasNext())
                    dispatch((SelectionKey)(it.next()));
                    selected.clear();
                }
        } catch (IOException ex) {
            /* ... */ }
        }

    

    void dispatch(SelectionKey k) {
        Runnable r = (Runnable) (k.attachment());
        if (r != null)
            r.run();
    } 
    
    
   class Acceptor implements Runnable { // inner
        public void run(){
            try{
                SocketChannel c = serversocketchannel.accept();
                if (c != null)
                    new Handler(selector, c);
            } catch (IOException ex) {
                /* ... */ }
        }
    }
    
   public static void main(String[] args) throws IOException {
	   NioReactor nioReactor =new NioReactor(22333);
	   nioReactor.run();
}
   
   
}
