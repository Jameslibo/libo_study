/**
 * @author Administrator 
 * @date 2019年5月28日
 * @version 1.0
 * <p>Company: https://www.anchnet.com</p>
 * @Copyright: © 2007-2019 | 上海安畅网络科技股份有限公司 版权所有 
 * 注意：本内容仅限于上海安畅网络科技股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */  
package com.stuady.controller;


import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.annotation.Bean;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.stuady.Log.DVSPOperateLog;
@Slf4j
@Controller
@RequestMapping("/biz")
public class SpittleCntroller {

    /**
     * 
     */
    public static final int DEFAULT_SPITTLES_PER_PAGE = 25;

    // 每页的大小
    private int spittlesPerPage = DEFAULT_SPITTLES_PER_PAGE;

    public int getSpittlesPerPage() {
        return spittlesPerPage;
    }

    public void setSpittlesPerPage(int spittlesPerPage) {
        this.spittlesPerPage = spittlesPerPage;
    }

    @RequestMapping(value = "/test", method = GET)
    public String test() {
    	
    	log.info(
				DVSPOperateLog.builder()
				.withOperatername("检查").withContent("根据证书核扣")
				.withResourcename("报关单E000022222")
				.build().toString()
				, "*****LIBO******");
    	
    	log.info("报关单E000022211111111s", DVSPOperateLog.builder()
				.withOperatername("检查").withContent("根据证书核扣")
				.withResourcename("报关单E000022222")
				.build());
    	
        String result = spittlesPerPage + " - test()";
        System.out.println(result);
        return "home";
    }

    @Bean
    public MBeanExporter mbeanExporter(SpittleCntroller spittleController) {
        MBeanExporter exporter = new MBeanExporter();
        Map<String, Object> beans = new HashMap<String, Object>();
        beans.put("spitter:name=SpittleController", spittleController);
        exporter.setBeans(beans);
        return exporter;
    }
}
