package libo.libo;

import javax.sql.DataSource;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.integration.config.xml.AnnotationConfigParser;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.stuady.Log.DVSPOperateLog;

@Slf4j
@SpringBootApplication
@MapperScan("com.myson.amp.db.mapper")
@EnableTransactionManagement
@ComponentScan("com.*")
public class RedissonApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(RedissonApplication.class, args);
	
		
	}
	@Bean
	Redisson redissonSentinel() {
		Config config = new Config();
		//支持单机，主从，哨兵，集群等模式
		
		//此为哨兵模式
	/*	config.useSentinelServers()
				.setMasterName("mymaster")
				.addSentinelAddress("redis://192.168.1.1:26379")
				.setPassword("123456");*/
		//集群模式
	/* config.useClusterServers() //这是用的集群server 
        .setScanInterval(2000) //设置集群状态扫描时间 
        .setMasterConnectionPoolSize(10000) //设置连接数 
        .setSlaveConnectionPoolSize(10000) 
        .addNodeAddress("127.0.0.1:6379");*/ 
		log.debug(
				DVSPOperateLog.builder()
				.withOperatername("检查").withContent("根据证书核扣")
				.withResourcename("报关单E000022222")
				.build()
				, "*****LIBO******");
		
		//单机模式
		//  config.useSingleServer().setAddress("redis://212.129.147.39:6379").setPassword("anchnet");
		  
         //(Redisson) Redisson.create(config);  
          //清空自增的ID数字  
         // RAtomicLong atomicLong = redisson.getAtomicLong(key);  
		
		return null;//(Redisson)Redisson.create(config);
	}
	
	// DataSource配置
		@Bean
		// @ConfigurationProperties(prefix = "spring.datasource")
		// @Bean(name = "dataSource")
		// @Qualifier(value = "dataSource")
		// @Primary
		@ConfigurationProperties(prefix = "c3p0")
		public DataSource dataSource() {
			// return new org.apache.tomcat.jdbc.pool.DataSource();
			return DataSourceBuilder.create().type(com.mchange.v2.c3p0.ComboPooledDataSource.class).build();
			// return getDataSource();
		}

		@Bean("sqlsessionfactory")
		public SqlSessionFactory sqlSessionFactoryBean() throws Exception {

			SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
			sqlSessionFactoryBean.setDataSource(dataSource());

			PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

			sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:/config/**/*.xml"));

			return sqlSessionFactoryBean.getObject();
		}

		@Bean
		public PlatformTransactionManager transactionManager() {
			return new DataSourceTransactionManager(dataSource());
		}


}